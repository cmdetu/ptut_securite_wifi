function swap(array: Array<any>, el1: number, el2: number) {
    let temp = array[el1]
    array[el1] = array[el2]
    array[el2] = temp
}
function stringToAscii(str: string) {
    let out: Array<number> = []
    for (let i = 0; i < str.length; ++i) {
        out.push(str.charCodeAt(i))
    }
    return out
}
function asciiToString(input: Array<number>) {
    let out = ""
    for (let i = 0; i < input.length; ++i) {
        out += String.fromCharCode(input[i])
    }
    return out
}
function RC4(key: Array<number>, input: Array<number>) {

    let tab: Array<number> = []
    //let key = "test"
    for (let i = 0; i < 256; ++i) {
        tab[i] = i

    }
    let j = 0

    for (let i = 0; i < 256; ++i) {
        j = (j + tab[i] + key[i % key.length]) % 256
        swap(tab, i, j)
    }
    //let input = "babar"
    let i = 0
    j = 0
    let out: Array<number> = []
    for (let a = 0; a < input.length; ++a) {
        i = (i + 1) % 256
        j = (j + tab[i]) % 256
        swap(tab, i, j)
        let cryptOct = tab[(tab[j] + tab[j]) % 256]
        let result = cryptOct ^ input[a]
        console.log(result)
        out.push(result)
    }
    return out
}



let inputString = <HTMLInputElement>document.getElementById("stringInput")
let inputArray = <HTMLInputElement>document.getElementById("asciiInput")
let runButton = <HTMLButtonElement>document.getElementById("run")
let outHistory = <HTMLUListElement>document.getElementById("history")
let arrayEnabled = <HTMLInputElement>document.getElementById("arrayEnabled")
let stringEnabled = <HTMLInputElement>document.getElementById("stringEnabled")
let secretInput = <HTMLInputElement>document.getElementById("secretInput")


runButton.addEventListener("click", () => {
    let str: string = inputString.value

    let input: Array<number> = []
    if (stringEnabled.checked) {
        input = stringToAscii(str)
    } else {
        input = JSON.parse(inputArray.value)
    }
    let out = RC4(stringToAscii(secretInput.value), input)
    let tr = document.createElement("tr")
    let date = new Date
    let thDate = document.createElement("td")
    thDate.textContent = `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`
    tr.appendChild(thDate)
    let thString = document.createElement("td")
    thString.textContent = asciiToString(out)
    tr.appendChild(thString)
    let thArray = document.createElement("td")
    thArray.textContent = `[${out.toString()}]`
    tr.appendChild(thArray)
    console.log(tr)
    outHistory.insertBefore(tr, outHistory.childNodes[0])
})