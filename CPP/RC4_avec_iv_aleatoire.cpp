#include <iostream>
#include <array>
#include <vector>
#include <iomanip>
#include <random>
#include <chrono>
#include <sstream>

using namespace std;

vector<uint8_t> initIV(const unsigned taille, default_random_engine & generator)
{
    uniform_int_distribution<uint8_t> aleatoire(0, 255);
    vector<uint8_t> IV;
    for(unsigned i(0); i < taille; ++i)
        IV.push_back(aleatoire(generator));
    return IV;
}

vector<uint8_t> keyFromHex(const string & str)
{
    vector<uint8_t> key;
    if(str.size() % 2 != 0) return key;
    istringstream istr(str);
    char hex1, hex2;
    while(istr.get(hex1))
    {
        istr.get(hex2);//str.size() % 2 == 0
        if(!isxdigit(hex1) || !isxdigit(hex2))
        {
            key.clear();
            return key;
        }
        ostringstream ostr2;
        ostr2 << hex1 << hex2;
        istringstream istr2(ostr2.str());
        unsigned val;//besoin d'un unsigned pour lire les 2 char
        istr2 >> hex >> val;
        key.push_back(uint8_t(val));
    }

    return key;
}

vector<uint8_t> RC4(const vector<uint8_t> & cle, const vector<uint8_t> & chaine)
{
    array<uint8_t, 256> tab;

    for(unsigned i(0); i < tab.size(); ++i)//init tab
        tab[i] = uint8_t(i);


    //KSA
    uint8_t j(0);//correspond a j0 dans le poly RC4 donc la boucle permet de calculer de j1 à j256 (car 255 itérations)
    for(unsigned i(0); i < tab.size(); ++i)
    {
        j = (j + tab[i] + cle[i % cle.size()]) % 256;
        swap(tab[i], tab[j]);
    }


    //PRGA
    uint8_t a(0), b(0);
    vector<uint8_t> tab2;
    for(unsigned i(0); i < chaine.size(); ++i)
    {
        a = (a + 1) % 256;
        b = (b + tab[a]) % 256;
        swap(tab[a], tab[b]);
        tab2.push_back((tab[(tab[a] + tab[b]) % 256] ) ^ chaine[i]);//XOR avec le texte clair
    }

    return tab2;
}

vector<uint8_t> messageToVec(const string & str)
{
    vector<uint8_t> vec;
    for(const char & c : str)
        vec.push_back(uint8_t(c));
    return vec;
}

int main()
{
    //Le vecteur d'initialisation doit être modifié a chaque nouvelle transmission

    default_random_engine generator(chrono::system_clock::now().time_since_epoch().count());

    string message("toto");
    string hexWepKey("2a6e20352a7e282249717d2e6b");//clé wep : 104 bits

    vector<uint8_t> wepKey(keyFromHex(hexWepKey));
    if(wepKey.size() == 0)
    {
        cerr << "cle WEP invalide" << endl;
        return -1;

    }
    vector<uint8_t> messageVec(messageToVec(message));

    vector<uint8_t> RC4key(initIV(3, generator));//IV : 24 bits (3 octets)

    RC4key.insert(RC4key.end(), wepKey.begin(), wepKey.end());// IV + WEPKEY : 128 bits (WEP-104)

    vector<uint8_t> tabChiffre(RC4(RC4key, messageVec));
    for(const uint8_t & val : tabChiffre)
        cout << hex << int(val) << endl;

    cout << endl << "-----" << endl << endl;

    vector<uint8_t> tabDechiffre(RC4(RC4key, tabChiffre));

    for(const uint8_t & val : tabDechiffre)
        cout << char(val) << endl;


    return 0;
}
