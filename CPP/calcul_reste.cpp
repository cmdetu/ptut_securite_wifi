/*
Calcul de reste de la division de polynomes � coefficients dans F2

R�alis� dans le cadre d'un projet de sciences de l'ing�nieur
Repris et adapt� pour un projet tutor� - DUT1 - Informatique - Aix en Provence
Auteur : Cl�ment Mathieu--Drif
*/

#include <iostream>
#include <string>

bool ckCharBin(const std::string & str)
{
    for(const char & c : str)
        if(c != '0' && c != '1')
            return false;
    return true;
}


int main()
{
    std::string diviseur, dividende;
    //entrees
    std::cout << "dividende : ";
    std::cin >> dividende;
    std::cout << "diviseur : ";
    std::cin >> diviseur;

    if(!ckCharBin(dividende) || !ckCharBin(diviseur))
    {
        std::cerr << "erreur de saisie" << std::endl;
        return -1;
    }

    unsigned i(0);
    while(i < dividende.size() && dividende[i] == '0')
        ++i;
    dividende = dividende.substr(i);

    i = 0;
    while(i < diviseur.size() && diviseur[i] == '0')
        ++i;
    diviseur = diviseur.substr(i);

    if(diviseur.size() > dividende.size())
    {
        std::cout << std::endl << "reste : " << dividende << std::endl;
        return 0;
    }

    i = 0;
    while(i <= dividende.length() - diviseur.length())
    {
        for(unsigned j(0); j < diviseur.length(); j++)
            dividende[i+j] = (diviseur[j] == dividende[i+j] ? '0' : '1'); //xor

        while(i < dividende.length() && dividende[i] != '1')//decalage de registre jusqu'au 1 suivant (sauter les 0 de gauche)
            ++i;
        std::cout << std::endl << "polynome : " << dividende;
    }

    std::cout << std::endl << "reste : " << dividende << std::endl;
    return 0;
}
