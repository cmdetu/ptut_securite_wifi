#include <iostream>
#include <vector>
#include <random>
#include <chrono>
#include <algorithm>


using namespace std;


vector<uint8_t> initIV(const unsigned taille, default_random_engine & generator)
{
    uniform_int_distribution<uint8_t> aleatoire(0, 255);
    vector<uint8_t> IV;
    for(unsigned i(0); i < taille; ++i)
        IV.push_back(aleatoire(generator));
    return IV;
}


typedef vector<vector<uint8_t>>::size_type size_type_uint8_t;
int main()
{
    default_random_engine generator(chrono::system_clock::now().time_since_epoch().count());
    vector<vector<uint8_t>> allIVs;
    vector<size_type_uint8_t> tailles;
    bool found;
    vector<vector<uint8_t>>::const_iterator it;
    vector<uint8_t> RC4key;
    for(unsigned i(0); i < 2; ++i)
    {
        found = false;
        while(!found)
        {
            RC4key = initIV(3, generator);//IV : 24 bits
            it = (find(allIVs.begin(), allIVs.end(), RC4key));
            if(it != allIVs.end())
                found = true;
            allIVs.push_back(RC4key);
        }
        tailles.push_back(allIVs.size() + 1);
        allIVs.clear();
    }
    size_type_uint8_t avg(accumulate(tailles.begin(), tailles.end(), size_type_uint8_t(0)) / tailles.size());
    cout << avg << endl;
    return 0;
}

