\documentclass[main.tex]{subfiles}
\begin{document}

\section{WPA2}
\subsection{Faille KRACK}
\subsubsection{Résumé de la situation}
Le protocoles WPA2 (chiffrement CCMP qui s'appuie sur AES) est, depuis l'abandon officiel du WEP par la Wi-Fi Alliance en 2004, le protocole le plus largement utilisé pour la sécurisation des réseau WIFI. Cependant, un type d'attaque nommé KRACK\footnote{KRACK ne permet de trouver ni le mot de passe d'un réseau WIFI protégé par WPA2 ni la clé de chiffrement mise en place lors du 4-way handshake} (Key Reinstallation Attacks) découvert par Mathy Vanhoef et Frank Piessens en 2017 permet, grâce à une configuration "homme du milieu" (man in the middle), de déchiffrer des données transmises sur un réseau protégé par le protocole WPA2.
Contrairement à d'autres attaques, l'existence de la vulnérabilité ne dépend pas des différentes implémentations du protocole mais se situe dans le protocole lui-même. C'est ici la phase de connexion d'un appareil au point d'accès qui est concernée, cette phase est appelée 4-way handshake (décrit dans la partie précédente).
Cette étape permet de définir avec quelle clé (PTK : Pairwise Transient Key - différente pour chaque session) les messages envoyés sur le réseau seront chiffrés. Les protocoles WPA2 personnel et entreprise sont concernés par cette vulnérabilité car ils utilisent tous deux un 4-way handshake.
Il existe différentes formes d'attaques KRACK, et nous prenons ici le parti de décrire la plus connue d'entre-elles qui opère sur le troisième message du 4-way handshake.

\subsubsection{Outils de détection}
Mathy Vanhoef a mis à disposition un ensemble de scripts permettant de détecter si un client ou un point d'accès est concerné par les attaques de type KRACK :
\\
\url{https://github.com/vanhoefm/krackattacks-scripts}

\newpage

\subsubsection{Attaque de l'homme du milieu (man in the middle : MITM)}

Une telle attaque consiste, pour un attaquant, à rediriger le trafic du réseau afin qu'il passe par sa machine (pour cela, il existe différentes méthodes : ARP Poisoning, au DNS Spoofing, ou encore la création d'un point d'accès WIFI...).
Cette configuration lui permet de lire les données échangées et dans certains cas de les modifier. Il peut également injecter des données sur le réseau.\footnote{Plus d'informations : \url{https://www.ionos.fr/digitalguide/serveur/securite/attaque-de-lhomme-du-milieu-apercu-du-modele/} et \url{https://us.norton.com/internetsecurity-wifi-what-is-a-man-in-the-middle-attack.html}}
\begin{figure}[htp]
   \includegraphics[scale=0.5]{man-in-the-middle-attack.png}
\end{figure}
\url{www.thesslstore.com/blog/man-in-the-middle-attack-2/}

\subsubsection{Associativité du XOR (eXclusive OR)}
algèbre de boole :
\newline
\textit{XOR est noté : \(\oplus\)}
\[(a \oplus b) \oplus c\]
\[ = ((a . \neg b + \neg a . b) . \neg c) + (\neg(a . \neg b + \neg a . b).c)\]
\[ = (a . \neg b . \neg c + \neg a . b . \neg c) + (\neg(a . \neg b) . \neg(\neg a . b).c)\]
\[ = (a . \neg b . \neg c + \neg a . b . \neg c) + ((\neg a + b) . (a + \neg b).c)\]
\[ = (a . \neg b . \neg c + \neg a . b . \neg c) + (\neg a . a . c) + (\neg a . \neg b . c) + (b . a . c) + (b . \neg b . c)\]
\[ = (a . \neg b . \neg c + \neg a . b . \neg c) + (\neg a . \neg b . c) + (b . a . c)\]
\[ = a . ((\neg b . \neg c) + (b.c)) + \neg a . (b . \neg c + \neg b . c)\]
\[ = a . \neg ( \neg (\neg b . \neg c) . \neg (b.c)) + \neg a . (b . \neg c  + \neg b . c)\]
\[ = a . \neg ((b + c) . (\neg b + \neg c)) + \neg a . (b . \neg c  + \neg b . c)\]
\[ = a . \neg (b . \neg b + b . \neg c + c . \neg b + c . \neg c) + \neg a . (b . \neg c + \neg b . c)\]
\[ = a . \neg (b . \neg c + c . \neg b) + \neg a . (b . \neg c + \neg b . c)\]
\[ = a \oplus (b \oplus c)\]

Donc \((a \oplus b) \oplus c = a \oplus (b \oplus c)\)


\subsubsection{Fonctionnement de l'attaque}

La mise en place de cette attaque débute par la création, par l'attaquant, d'un clone du point d'accès WIFI sur un canal différent (il y a par exemple 11 canaux sur la bande des 2.4GHz). Les données transmises seront ensuite relayées sur le canal réel du point d'accès.
Dans cette situation, l'attaquant peut intercepter les données transmises mais est incapable de les déchiffrer.
Voici un extrait de \textit{Operating Channel Validation: Preventing Multi-ChannelMan-in-the-Middle Attacks Against Protected Wi-Fi Networks}\footnote{\url{https://papers.mathyvanhoef.com/wisec2018.pdf}} par Mathy Vanhoef, Nehru Bhandaru et Thomas Derham :
\newline
\begin{displayquote}
\textit{"3.3  Channel Switch Announcements (CSAs)We discovered a novel method to force clients into connecting to the rogue AP. In particular, an adversary can forge Channel SwitchAnnouncements (CSAs) to force a client to switch to the rogue AP’schannel. Normally these announcements are sent when the AP is switching to a different channel. For instance, when an AP operates on certain 5 GHz channels and detects (weather) radar pulses, it must switch to a different channel to comply with regulations.Channel switch announcements can be broadcasted using three different frames. The first is by including a CSA element inside a beacon. The second method is by including a CSA element inside a probe response. And the third technique is to send action frames with a CSA element to associated clients. The first two methods use unprotected management frames, and the third method is protected when MFP is enabled. As a result, even when MFP is used, an adversary can forge CSA elements inside beaconsand probe responses to force clients to switch channels."}
\end{displayquote}

La seconde étape consiste à transmettre les trois premiers messages du 4-way handshake puis à bloquer le quatrième. Du point de vue du client, la procédure de connexion est terminée mais le point d'accès n'a pas reçu le dernier message. Dans cette situation, le point d'accès procède à la ré-émission du troisième message qui est traité à nouveau, or, le client ayant déjà installé la clé de chiffrement (PTK), le quatrième message (envoyé pour la seconde fois) est désormais chiffré. L'attaquant doit ici bloquer ce message chiffré qui serait rejeté par le point d'accès et retransmettre la première version du quatrième message (non chiffré) au point d'accès afin qu'il installe la clé de chiffrement à son tour (l'utilisation de ce message 4 non chiffré implique que le numéro de paquet n'est pas le plus récent : toutefois de nombreuses implémentations l'acceptent tout de même car il n'a jamais été utilisé dans une trame de réponse de la part du client mais seulement en émission depuis le point d'accès).
À cette étape, la clé est réinstallée par le client, ce qui entraîne la réinitialisation du compteur de trames (Nonce) et donc, l'émission de trames avec un Nonce déjà utilisé. Cela permet de déchiffrer les trames envoyées dans la mesure où le keystream est utilisé pour la seconde fois. L'opération XOR bit à bit entre le message 4 chiffré et celui non chiffré donne:

\textit{note : Nous utilisons ici les mêmes notations que dans le document décrivant le fonctionnement du chiffrement RC4}
\[C = G \oplus M\]
\begin{center}
    \[C \oplus M = (G \oplus M) \oplus M\]
    donc par associativité : 
    \((G \oplus M) \oplus M = G \oplus (M \oplus M) = G\)
\end{center}

\begin{figure}[htp]
   \includegraphics[scale=0.43]{wpa.png}
\end{figure}
\textit{Représentation simplifiée du chiffrement avec WPA2-CCMP}
\newline

Nous connaissons alors le keystream utilisé pour les trames chiffrées correspondant à ce Nonce, et elles deviennent donc facilement déchiffrables.


L'utilisation d'un chiffrement sur la couche transport (SSL) permet d'éviter que les données soient déchiffrables grâce à une attaque de type KRACK. Il est cependant possible de contourner cette sécurité grâce à sslstrip\footnote{\url{https://tools.kali.org/information-gathering/sslstrip}} comme présenté par Mathy Vanhoef.\footnote{\url{https://www.youtube.com/watch?v=Oh4WURZoR98}}
Il est donc préférable d'utiliser du matériel à jour (non exposé à ce type d'attaques).



\subsubsection{Cas particuler : wpa\_supplicant 2.4 et 2.5}
wpa\_supplicant est un client WIFI utilisé par linux et android.
Après l'installation de la clé de chiffrement par le noyau linux, wpa\_supplicant ne conserve pas de copie de cette clé et l'efface avec des 0 dans la mémoire (conseillé par la norme ieee 802.11).
\begin{minted}{c}
    os_memset(sm->ptk.tk,0, WPA_TK_MAX_LEN);
\end{minted}
Lors de la réinstallation, c'est la clé appelée "all-zero key" qui est installée, et à partir de cet instant, toutes les trames transmises en unicast sont chiffrées avec cette nouvelle clé.
Ainsi, les données sont facilement déchiffrables.
C'est l'exécution à deux reprises de la fonction \mintinline{c}{wpa_supplicant_install_ptk()} qui est à l'origine de la vulnérabilité.

Voici une partie du commit\footnote{\url{https://w1.fi/cgit/hostap/commit/?id=ad00d64e7d8827b3cebd665a0ceb08adabf15e1e}} qui empêche l'installation d'une clé "all-zero" dans la version 2.6 de wpa\_supplicant:
\begin{minted}{c}
+	if (!sm->tk_to_set) {
+		wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG,
+			"WPA: Do not re-install same PTK to the driver");
+		return 0;
+	}
\end{minted}
puis
\begin{minted}{c}
+	sm->tk_to_set = 0;
\end{minted}
qui empêche une réinstallation de la clé si la fonction est exécutée plus d'une fois.
wpa\_supplicant 2.6 est cependant vulnérable à un autre type d'attaque de la famille KRACK qui agit sur le premier message du 4-way handshake.


\subsubsection{Impact}
Dans la pratique, cette attaque permet de déchiffrer les données transmises après l'exécution du 4-way handshake, or, ces données ne sont pas "intéressantes" pour un attaquant. Il est cependant possible de forcer la "désautentification" du client, le système d'exploitation procède alors à un nouveau 4-way handshake afin de se reconnecter au point d'accès.
Les données transmises après cette reconnexion pourront être déchiffrées (car les connexions TCP ne sont pas fermées pendant ce processus).

Dans le cas de l'installation d'une "all-zero key" il est possible de calculer le keystream utilisé et donc de déchiffrer les données transmises pour chaque valeur de nonce.
Notons tout de même ceci :
\textit{"The real AP will then ignore data frames from the victim (because it
fails to decrypt them)."} (ceci nous a été précisé lors d'une conversation avec Mathy Vanhoef)


\subsubsection{Informations complémentaires}
Il est possible de contrôler le nombre de valeurs de nonce qui seront réutilisées en choisissant l'instant de réémission du troisième message du 4-way handshake.
\textit{"You can delay the key reinstallation to, in principle, decrypt
arbitrary many frames. E.g. you let the victim send N frames, and only
then trigger to key reinstallation, after which you can decrypt N
frames."}(ceci nous a également été précisé par Maty Vanhoef).

De plus, si nous ne disposons pas de la version non chiffrée d'une trame mais seulement de deux trames chiffrée avec une réutilisation du Nonce, il est possible d'utiliser des méthodes telles que le crib dragging\footnote{\url{http://www.cribdrag.com/about.html}} pour déchiffrer les données.

Voici ce qui nous a été précisé par Mathy Vanhoef
\textit{"Here we assume that if there are two message with a reused nonce, we
know the plaintext of one of them, e.g. based on the length of the
frame we can predict what type of packet it is, and this packet may
have a very predictable content. For example, traffic analysis can be
used to detect (based on encrypted frames) which plaintext HTTP website
a victim is visiting, allowing is to predict the content of a lot of
frames. This predicted plaintext can then be used to decrypt the other
frames with the reused nonce."}
\subsubsection{Correctifs}
La majorité des appareils qui étaient encore maintenus lors de cette découverte ont reçu une mise à jour de sécurité empêchant l'exploitation de KRACK.
Il faut cependant veiller à ce que les points d'accès (également vulnérables) aient été mis à jour dans la mesure où ce processus n'est généralement pas automatique.

\subsection{Faille Kr00k}

Une vulnérabilité permettant de déchiffrer les données transmises sur les réseaux utilisant le protocole WPA2 a été découverte par la société ESET en 2019 et rendue publique le 26 février 2020 (RSA Conference).
Les chercheurs d'ESET ont découvert cette vulnérabilité alors qu'ils cherchaient à déterminer si l'Amazon echo 2 était concerné par une attaque de type KRACK.
Nommée Kr00k,\footnote{\url{https://www.welivesecurity.com/wp-content/uploads/2020/02/ESET\_Kr00k.pdf}}
elle concerne la phase de désassociation d'une station et d'un point d'accès qui peut être amorcée de plusieurs manières.

\subsubsection{Fonctionnement}

Après une désassociation, la clé de chiffrement utilisée est effacée dans la mémoire de la puce WIFI et est remplacée par des 0. Il est cependant possible que des données se trouvent encore dans le tampon d'émission (d'une capacité de 32Kb). Ces trames sont chiffrées avec la clé en mémoire ("all-zero key") et envoyées sur le réseau. Les données sont alors facilement déchiffrables si elles ne sont pas chiffrées à un niveau supérieur (SSL par exemple sur la couche transport).

Les désassociations se produisent naturellement lorsque la station passe d'un point d'accès à un autre, par exemple. Il est cependant possible de forcer cette désassociation par des trames spéciales : "802.11 management frames" qui ne sont pas chiffrées et peuvent donc être envoyées par un attaquant.

\begin{figure}[htp]
   \includegraphics[scale=0.45]{KR00K.png}
\end{figure}
\textit{Source : RSA Conference 2020, ESET} \\

Le contenu des trames récupérées est alors aléatoire car il dépend de l'instant auquel se produit la désassociation. La répétition du processus permet d'obtenir un grand nombre de trames et donc potentiellement des données intéressantes pour un attaquant.

\subsubsection{Appareils concernés}
Les puces WIFI touchées sont celles des fabricants Broadcom et Cypress, ce qui concerne plus d'un milliard d'appareils (clients et points d'accès).
Voici une liste non-exhaustive des modèles concernés :
\begin{itemize}
    \item Amazon Echo 2nd gen
    \item Amazon Kindle 8th gen
    \item Apple iPad mini 2
    \item Apple iPhone 6, 6S, 8, XR
    \item Apple MacBook Air Retina 13-inch 2018
    \item Google Nexus 5
    \item Google Nexus 6
    \item Google Nexus 6P
    \item Raspberry Pi 3
    \item Samsung Galaxy S4 GT-I9505
    \item Samsung Galaxy S8
    \item Xiaomi Redmi 3S
    \item Asus RT-N12
    \item Huawei B612S-25d
    \item Huawei EchoLife HG8245H
    \item Huawei E5577Cs-321
\end{itemize}

Si l'attaque est menée sur un point d'accès, ce sont les trames envoyées par ce dernier qui seront déchiffrables même si le client connecté n'est pas vulnérable.

\subsubsection{Correctifs}

Des correctifs logiciels ont été développés et déployés en 2019 et 2020 par les fabricants des puces WIFI concernées pendant la période de 120 jours octroyée par ESET avant que Kr00k ne soit rendue public.

\subsection{Autres attaques}
Il existe d'autres attaques visant les réseaux utilisant le protocole WPA2\footnote{\url{https://www.privateinternetaccess.com/blog/pmkid-dumping-wifi-password-attacks-are-easier-than-previously-thought/}}. Certains logiciels implémentent des attaques par force brute qui ont pour but de trouver le mot de passe sécurisant le réseau en forçant un client à se déconnecter et à procéder à un nouveau 4-way handshake. Ce type d'attaque est très rapide sur les réseaux protégés par un mot de passe faible susceptible de se trouver dans une liste de mots de passe appelée "dictionnaire".
Le protocole WPA3 ayant pour objectif d'empêcher les attaques connues sur le WPA2 a été introduit par la WIFI Alliance en 2018 et commence à être utilisé:

\begin{displayquote}
\textit{WPA3-Personal brings better protections to individual users by providing more robust password-based authentication, even when users choose passwords that fall short of typical complexity recommendations. This capability is enabled through Simultaneous Authentication of Equals (SAE), which replaces Pre-shared Key (PSK) in WPA2-Personal. The technology is resistant to offline dictionary attacks where an adversary attempts to determine a network password by trying possible passwords without further network interaction.\footnote{\url{https://www.wi-fi.org/discover-wi-fi/security}}}
\end{displayquote}

Il est à noter que Mathy Vanhoef et Eyal Ronen ont découvert une attaque\footnote{\url{https://wpa3.mathyvanhoef.com/}} visant le "Dragonfly handshake" utilisé par le WPA3 permettant de trouver le mot de passe d'un réseau utilisant ce protocole. Des correctifs ont cependant déjà été apportés et l'utilisation de WPA3 en remplacement de WPA2 est conseillée (si possible) du fait de la complexité des attaques par lesquelles ce protocole est concerné.

\end{document}

