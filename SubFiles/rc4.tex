\documentclass[main.tex]{subfiles}

\begin{document}
\subsection{RC4}
RC4 (Rivest Cipher 4) est un algorithme de chiffrement par flot crée en 1987 par Ronald Rivest.
Ce document décrit son fonctionnement au travers d'un exemple.\\

RC4 utilise deux fonction, KSA (Key Scheduling Algorithm) et PRGA (Pseudo Random Generation Algorithm) pour g\'en\'erer un flot de bits pseudo aléatoire qui sera utilis\'e pour le chiffrement.
De manière g\'en\'erale, la taille du tableau à initialiser est de \(n = 2^8 = 256\). Les éléments de ce tableau sont des mots de \(n\) bits
Dans notre cas, le tableau contiendra donc 256 éléments de 1 octet.\\

\textit{"For real applications, n=8 is used as it is a good trade-off between security and memory requirements, and it is also natural and easy to implement"}
{\tiny Evaluation of RC4 Stream Cipher  - Ed Dawson, Helen Gustafson,  Matt Henricksen, Bill Millan}\\

Remarque 1 : les implémentations fournies dans ce document (en C++) utilisent n = 256.\\

Remarque 2 : les tableaux représentés dans ce document le sont avec l'élément d'indice \(i = 0\) à gauche et celui d'indice \(i = tailleDuTableau - 1\) à droite.

\noindent Avant l'ex\'ecution de ces deux fonctions, le tableau est initialisé ainsi :

\begin{minted}{c++}
    array<uint8_t, 256> tab;
    for(unsigned i(0); i < tab.size(); ++i)
        tab[i] = uint8_t(i);
\end{minted}

Soit la permutation identité de \(Sym(n)\), \(id_n = (0, 1, ... , n - 1)\)\\\\


\noindent \(j_i \in \mathbb{Z}/n\mathbb{Z}\)\\
\noindent \(\hat{j_i} \in \mathbb{Z}/n\mathbb{Z}\)\\

\noindent \(\sigma_i \in Sym(n)\)\\
\noindent \(\hat{\sigma_i} \in Sym(n)\)\\

\noindent les \(K_i \in \mathbb{Z}/n\mathbb{Z}\) sont les caractères de la cl\'e K de taille \(l \in [1, n]\)\\
\(id_n = (0, 1, ..., n - 1)\)\\

\subsection{KSA}
Après initialisation, le vecteur est parcouru une première fois par l’indice \(i\) afin d'appliquer une série de permutations. Les permutations effectuées par KSA sont calculées en fonction de la clé \(K\).\\

\noindent Les permutations sont définies par ces relations :

\[
j_i = \begin{cases} 0 & \mbox{si i = 0}\\ j_{i - 1} + \sigma_{i - 1}(i - 1) + K_{i - 1 \mod l} & \mbox{si \(1 \leq i \leq n\)} \end{cases}
\]\\

\[
\sigma_i = \begin{cases} id_n & \mbox{si i = 0}\\ (i - 1, j_i) \circ \sigma_{i - 1} & \mbox{si \(1 \leq i \leq n\)} \end{cases}
\]

\noindent l'implémentation du KSA est la suivante :
\begin{minted}{c++}
    uint8_t j(0);
    for(unsigned i(0); i < tab.size(); ++i)
    {
        j = (j + tab[i] + cle[i % cle.size()]) % 256;
        swap(tab[i], tab[j]);
    }
\end{minted}

\noindent où
\begin{minted}{c++}
    j = (j + tab[i] + cle[i % cle.size()]) % n
\end{minted}
correspond à
\[j_i = j_{i - 1} + \sigma_{i - 1}(i - 1) + K_{i - 1 \mod l}\]\\

Puisque \(\sigma_0 = id_n\) et que cela correspond à la première initialisation du tableau, la boucle peut commencer à \(\sigma_1\). La permutation effectuée par le programme lorsque \(i = 0\) correspond donc à \(\sigma_1\). Le programme effectue alors \(n-1\) permutations.
La \({i}^{eme}\) permutation de l'algorithme correspond alors à la \({i+1}^{eme}\) permutation calculée avec la relation précédente. L'indice \mintinline{c++}{i} de l'implémentation varie donc entre \(0\) et \(n - 1\).\\

Voici un exemple avec \mintinline{c++}{n = 5}, \mintinline{c++}{K = } \begin{tabular}{|c|c|c|} \hline 4 & 2 & 1\\ \hline \end{tabular} et \mintinline{c++}{chaine = } \begin{tabular}{|c|c|c|} \hline 0 & 1 & 2\\ \hline \end{tabular}\\

\noindent\(j_0 = 0\)\\
\(\sigma_0 = id_5\)\\

\noindent\(j_1 = j_0 + \sigma_0(0) + K_0 = 0 + 0 + 4 = 4\)\\
\(\sigma_1 = (0, 4) \circ id_5 = (0, 4)\)\\

\noindent\(j_2 =  j_1 + \sigma_1(1) + K_1 = 4 + 1 + 2 = 7 \equiv 2 \mod 5\)\\
\(\sigma_2 = (1, 2) \circ (0, 4)\)\\

\noindent\(j_3 = j_2 + \sigma_2(2) + K_2 = 2 + 1 + 1 = 4\)\\
\(\sigma_3 = (2, 4) \circ ((1, 2) \circ (0, 4))\)\\

\noindent\(j_4 = j_3 + \sigma_3(3) + K_0 = 4 + 3 + 4 = 11 \equiv 1 \mod 5\)\\
\(\sigma_4 = (3, 1) \circ ((2, 4) \circ ((1, 2) \circ (0, 4)))\)\\

\noindent\(j_5 = j_4 + \sigma_4(4) + K_1 = 1 + 1 + 2 = 4\)\\
\(\sigma_5 = (4, 4) \circ ((3, 1) \circ ((2, 4) \circ ((1, 2) \circ (0, 4))))\)\\

\noindent ce qui correspond donc à :\\
\begin{center}
    \begin{tabular}{|c|c|c|c|c|} \hline 0 & 1 & 2 & 3 & 4\\ \hline \end{tabular}\\
    -\\
    \begin{tabular}{|c|c|c|c|c|} \hline 4 & 1 & 2 & 3 & 0\\ \hline \end{tabular}\\
    -\\
    \begin{tabular}{|c|c|c|c|c|} \hline 4 & 2 & 1 & 3 & 0\\ \hline \end{tabular}\\
    -\\
    \begin{tabular}{|c|c|c|c|c|} \hline 4 & 2 & 0 & 3 & 1\\ \hline \end{tabular}\\
    -\\
    \begin{tabular}{|c|c|c|c|c|} \hline 4 & 3 & 0 & 2 & 1\\ \hline \end{tabular}\\
    -\\
    \begin{tabular}{|c|c|c|c|c|} \hline 4 & 3 & 0 & 2 & 1\\ \hline \end{tabular}\\
\end{center}
les valeurs de j calculées par l'algorithme pour ce même exemple sont :\\
i = 0, j = 4\\
i = 1, j = 2\\
i = 2, j = 4\\
i = 3, j = 1\\
i = 4, j = 4\\

\noindent et les permutations qui en découlent sont de la forme : \mintinline{c++}{swap(tab[i], tab[j])}
soit (0, 4), (1, 2), (2, 4), (3, 1) et (4, 4) (dans l'ordre d'application) ce qui correspond à ce qui a été calculé précédemment.

\newpage

\subsection{PRGA}
PRGA (Pseudo-Random Generation Algorithm) est un générateur pseudo aléatoire qui effectue des permutations sur un tableau \mintinline{c++}{tab} pour en initialiser un second \mintinline{c++}{tab2} (appelé "keystream"). A chaque itération, PRGA échange deux éléments du tableau \mintinline{c++}{tab} (qui est le résultat du KSA). Le nombre d'itérations dépend de la taille du message à chiffrer
Le tableau mélangé \mintinline{c++}{tab2} est utilisé pour chiffrer le message à l'aide de l'opération XOR (OU exclusif) entre le vecteur mélangé et le message à chiffrer. Chaque élément du tableau est déplacé au moins une fois toutes les \(n = 256\) itérations.\\

\noindent implémentation en C++:
\begin{minted}{c++}
    uint8_t a(0), b(0);
    vector<uint8_t> tab2;
    for(unsigned i(0); i < chaine.size(); ++i)
    {
        a = (a + 1) % 256;
        b = (b + tab[a]) % 256;
        swap(tab[a], tab[b]);
        tab2.push_back((tab[(tab[a] + tab[b]) % 256]));
    }
\end{minted}

Les permutations appliquées sur \mintinline{c++}{tab} sont définies par les relation :

\[
\hat{j_i} = \begin{cases} 0 & \mbox{si i = 0}\\ j_{i - 1} + \hat{\sigma}_{i - 1}(i) & \mbox{si \(i \geq 1\)} \end{cases}
\]\\

\[
\hat{\sigma}_i = \begin{cases} \sigma_n & \mbox{si i = 0}\\ (i, j_i) \circ \hat{\sigma}_{i - 1} & \mbox{si \(i \geq 1\)} \end{cases}
\]

\(i \in [0, l - 1]\)\\

les \(z_i \in \mathbb{Z}/n\mathbb{Z}\) sont les éléments du second tableau :

\[z_i = \hat{\sigma}_{i + 1}(\hat{\sigma}_{i + 1}(i) + \hat{\sigma}_{i + 1}(j_{i + 1}))\]

La taille finale du tableau \mintinline{c++}{tab2} est \mintinline{c++}{chaine.size()} \( = l\)
\newpage

Suite de l'exemple avec \mintinline{c++}{n = 5}, \mintinline{c++}{K = } \begin{tabular}{|c|c|c|} \hline 4 & 2 & 1\\ \hline \end{tabular} et \mintinline{c++}{chaine = } \begin{tabular}{|c|c|c|} \hline 0 & 1 & 2\\ \hline \end{tabular}\\

\noindent\(j_0 = 0\)\\
\(\hat{\sigma}_0 = \sigma_5\)\\

\noindent\(j_1 = j_0 + \sigma_0(1) = 0 + 3 = 3\)\\
\(\hat{\sigma}_1 = (1, 3) \circ \sigma_5\)\\

\noindent\(j_2 =  j_1 + \sigma_1(2) = 3 + 0 = 3\)\\
\(\hat{\sigma}_2 = (2, 3) \circ ((1, 3) \circ \sigma_5)\)\\

\noindent\(j_3 =  j_2 + \sigma_2(3) = 3 + 0 = 3\)\\
\(\hat{\sigma}_3 = (3, 3) \circ ((2, 3) \circ ((1, 3) \circ \sigma_5))\)\\

\noindent\(j_4 =  j_3 + \sigma_3(4) = 3 + 1 = 4\)\\
\(\hat{\sigma}_3 = (4, 4) \circ ((3, 3) \circ ((2, 3) \circ ((1, 3) \circ \sigma_5)))\)\\

\noindent les différents états de \mintinline{c++}{tab} sont :\\
\begin{center}
    \begin{tabular}{|c|c|c|c|c|} \hline 4 & 3 & 0 & 2 & 1\\ \hline \end{tabular}\\
    -\\
    \begin{tabular}{|c|c|c|c|c|} \hline 4 & 2 & 0 & 3 & 1\\ \hline \end{tabular}\\
    -\\
    \begin{tabular}{|c|c|c|c|c|} \hline 4 & 2 & 3 & 0 & 1\\ \hline \end{tabular}\\
    -\\
    \begin{tabular}{|c|c|c|c|c|} \hline 4 & 2 & 3 & 0 & 1\\ \hline \end{tabular}\\
    -\\
    \begin{tabular}{|c|c|c|c|c|} \hline 4 & 2 & 3 & 0 & 1\\ \hline \end{tabular}\\
\end{center}
\subsection{Fin de l'exemple}

Calculons les valeurs des éléments de \mintinline{c++}{tab2} (\(z_i\))

\(z_0 = \hat{\sigma}_1(\hat{\sigma}_1(1) + \hat{\sigma}_1(j_1)) = \hat{\sigma}_1(\hat{\sigma}_1(1) + \hat{\sigma}_1(3)) = \hat{\sigma}_1(2 + 3) = \hat{\sigma}_1(0) = 4\)

\(z_1 = \hat{\sigma}_2(\hat{\sigma}_2(2) + \hat{\sigma}_2(j_2)) = \hat{\sigma}_2(\hat{\sigma}_2(2) + \hat{\sigma}_2(3)) = \hat{\sigma}_2(3 + 0) = \hat{\sigma}_2(3) = 0\)

\(z_2 = \hat{\sigma}_3(\hat{\sigma}_3(3) + \hat{\sigma}_3(j_3)) = \hat{\sigma}_3(\hat{\sigma}_3(3) + \hat{\sigma}_3(3)) = \hat{\sigma}_3(0 + 0) = \hat{\sigma}_3(0) = 4\)\\

Après \mintinline{c++}{chaine.size()} itérations, les éléments de \mintinline{c++}{tab2} sont :
\begin{tabular}{|c|c|c|} \hline 4 & 0 & 4\\ \hline \end{tabular}\\

la fin du processus de chiffrement consiste en un XOR bit à bit (défini plus loin) entre \mintinline{c++}{tab2} et \mintinline{c++}{chaine}.\\

\begin{minted}{c++}
    for(unsigned i(0); i < chaine.size(); ++i)
        tab2[i] = tab2[i] ^ chaine[i];
\end{minted}

Les implémentations du PRGA et de l'étape finale (XOR) peuvent être regroupées ainsi :

\begin{minted}{c++}
    uint8_t a(0), b(0);
    vector<uint8_t> tab2;
    for(unsigned i(0); i < chaine.size(); ++i)
    {
        a = (a + 1) % 256;
        b = (b + tab[a]) % 256;
        swap(tab[a], tab[b]);
        tab2.push_back((tab[(tab[a] + tab[b]) % 256]) ^ chaine[i]);
    }
\end{minted}

Le XOR (ou exclusif) est noté \(\oplus\) (et \mintinline{c++}{^} en C++) et est défini ainsi :\\
\begin{center}
    \begin{tabular}{|c|c|c|}
    \hline
    a & b & a XOR b\\
    \hline
    0 & 0 & 0\\
    \hline
    0 & 1 & 1\\
    \hline
    1 & 0 & 1\\
    \hline
    1 & 1 & 0\\
    \hline
    \end{tabular}\\
\end{center}

\noindent \mintinline{c++}{tab2 ^ chaine}\\

\mintinline{c++}{tab2} : \begin{tabular}{|c|c|c|} \hline 4 & 0 & 4\\ \hline \end{tabular}\\

\mintinline{c++}{chaine} : \begin{tabular}{|c|c|c|} \hline 0 & 1 & 2\\ \hline \end{tabular}\\

message chiffré : \begin{tabular}{|c|c|c|} \hline 4 XOR 0 & 0 XOR 1 & 4 XOR 2\\ \hline \end{tabular}\\

message chiffré : \begin{tabular}{|c|c|c|} \hline 4 & 1 & 6\\ \hline \end{tabular}






\subsection {Déchiffrement}

Le déchiffrement est très simple dans la mesure où il utilise la même fonction que celle que nous venons de décrire. La raison pour laquelle le chiffrement et le déchiffrement utilisent le même algorithme découle du fait que la fonction correspondante est involutive.

\subsubsection{Une fonction involutive est bijective}

Soit \( f : E \mapsto E \) une fonction involutive.\\
Soient \( x \)  et \( y \) deux éléments de E

\[  f(x) = f(y) \Rightarrow f( f(x) ) = f( f(y) ) \Rightarrow x = y \]

\noindent \( f \) est injective

\[ f(x) = y \Rightarrow f( f(x) ) = f(y) \Rightarrow x = f(y) \]

\noindent\( f \) est surjective et donc \( f \) est bijective

\subsubsection{Application au RC4}

Soit \( f : E \mapsto E \) avec \( f_{G}(M) = M \oplus G \) et E l'ensemble des messages (chiffrés ou non chiffrés).\\
M est le message à chiffrer, G est le tableau pseudo aléatoire dépendant de la clé de chiffrement (sortie du PRGA) et C est le message chiffré donc \(C = f_{G}(M) = M \oplus G\).\\
La fonction \( f \) est utilisée dans le chiffrement RC4 de la manière suivante :\\

\[ f_{G}(M) = M \oplus G = C \]\\

\noindent Montrons que \( f_{G}(C) = M \)\\

\noindent \( f_{G}(C) = C \oplus G = (M \oplus G) \oplus G \)\\
\(= \neg ( \neg M . G + M . \neg G ) . G + ( \neg M . G + M . \neg G ) . \neg G\)\\
\(= (\neg ( \neg M . G ) . \neg ( M . \neg G )) . G + ( \neg M . G . \neg G ) + ( M . \neg G . \neg G )\)\\
\(= (\neg ( \neg M . G ) . \neg ( M . \neg G )) . G + ( M . \neg G )\)\\
\(= (( M + \neg G ) . ( \neg M + G )) . G + ( M . \neg G )\)\\
\(= ( ( M . \neg M ) + ( M . G ) + ( \neg G . \neg M ) + ( \neg G . G ) ) . G + ( M . \neg G )\)\\
\(= (( M . G ) + ( \neg G . \neg M )) . G + ( M . \neg G )\)\\
\(= ( M . G . G ) + ( \neg G . \neg M . G ) + ( M . \neg G )\)\\
\(= ( M . G ) + ( M . \neg G )\)\\
\(= M . ( G + \neg G )\)\\
\(= M . 1 = M\)\\

\noindent Nous avons donc :

\[ f_{G}(M) = M \oplus G = C \]

\noindent et

\[ f_{G}(C) = C \oplus G = M \]

\noindent donc la fonction est involutive.

\[ f_{G} \circ \ f_{G} = Id_{E} \]

\noindent Dans RC4, la fonction de chiffrement est la m\^eme que celle de d\'echiffrement.

\subsubsection{Conséquence}

\noindent Nous avons vu précédemment qu'une application involutive est bijective. De ce fait, dans le cas de RC4, deux messages différents ne peuvent pas donner lieu au même message chiffré pour un keystream donné. Cette propri\'et\'e d\'ecoule de l'injectivit\'e.

\end{document}
