\documentclass[main.tex]{subfiles}
\begin{document}

\setcounter{MaxMatrixCols}{20}

\section{WPA2}

\subsection{Historique}

Suite à la découverte des vulnérabilités du WEP une nouvelle version du système de sécurisation des réseaux WIFI était nécessaire.
La norme IEEE 802.11i est alors rédigée.
Une première version, le WPA a été publiée en 2003 alors que la nouvelle norme était encore en rédaction.
Finalement en 2004 la norme IEEE 802.11i est terminée et donne naissance au WPA2 encore largement utilisé aujourd'hui.

Il existe deux versions de WPA2 :
\begin{itemize}
    \item WPA2 personnel qui se base sur un secret partagé
    \item WPA2 entreprise qui utilise un serveur d'authentification (comme eduroam).
\end{itemize}

En 2018 la wifi Alliance a annoncé WPA3 corrigeant des problèmes de sécurité de WPA2 (voir à ce sujet la partie consacrée à la faille KRACK). WPA3 a cependant déjà été compromis par Mathy Vanhoef (à l'origine de la faille KRACK du WPA2) et Eyal Ronen, qui ont présenté en avril 2019 l'attaque DragonBlood se basant sur le Dragonfly Handshake utilisé par ce protocole. Des correctifs ont depuis été publiés.


\subsection{Chiffrement et modes de chiffrement}

\subsubsection{TKIP}
Temporal Key Integrity Protocol, utilise le chiffrement RC4. Ce type de connexion est considérée comme non sécurisée et est uniquement utilisé pour assurer la compatibilité avec de vieux équipements.
TKIP n'est pas traité en détail dans ce document.

\subsubsection{CCMP}
Counter Mode with Cipher Block Chaining Message Authentication Code Protocol (CCMP), basé sur le standard AES (Advanced Encryption Standard), permet une protection supérieure et une vérification d'intégrité plus efficace que TKIP. L'utilisation de cette méthode de chiffrement est nécessaire pour dépasser un débit de 54 Mbit/s (spécification 802.11n). \\
CCMP utilise une clé de 128 bits et un numéro de paquet \(PN\) de 48 bits qui n'est jamais réutilisé et évite les attaques de type rejeu.
Ce protocole encapsule les données dans un MPDU (MAC Protocol Data Unit). \\
CCMP utilise le mode CTR pour le chiffrement des données avec AES et CBC-MAC pour contrôler l'intégrité des messages. Commençons par décrire ces deux modes. \footnote{\url{https://en.wikipedia.org/wiki/Block\_cipher\_mode\_of\_operation}}

\subsubsection{CTR - Counter mode}

Ce mode ne crée pas de lien entre les blocs de données contrairement au mode CBC décrit dans la partie suivante. Cette particularité peut permettre d'effectuer le chiffrement en parallèle car le traitement d'un bloc ne dépend pas de celui des blocs précédents.

Notons \(M_i\) le \(i^{eme}\) bloc non chiffré, \(C_i\) le \(i^{eme}\) bloc chiffré. La fonction de chiffrement est notée \(AES_k\) avec \(k\) la clé de chiffrement.

\begin{figure}[htp]
    \centering
    \includegraphics[scale=0.59]{CTR_encrypt.svg.png}
\end{figure}
\textit{(source : Wikipédia)} \\

\[C_i = AES_k(Nonce || Counter_i) \oplus M_i\]
Où || \textit{est l'opérateur de concaténation. Et \(\oplus\) représente l'opérateur XOR.}

\begin{figure}[htp]
\centering
    \includegraphics[scale=0.59]{CTR_decrypt.svg.png}
\end{figure}
\textit{(source : Wikipédia)}
\[M_i = C_i \oplus AES_k(Nonce || C_i)\]

Dans ce mode, comme pour le chiffrement RC4 par exemple, la fonction de chiffrement est la même que celle de déchiffrement (voir la partie sur RC4).

\[C_i = AES_k(Nonce || Counter_i) \oplus M_i\]
ce qui nous donne donc :
\[AES_k(Nonce || Counter_i) \oplus C_i\]
\[= AES_k(Nonce || Counter_i) \oplus AES_k(Nonce || Counter_i) \oplus M_i = M_i\]

CCMP utilise une valeur appelée \textit{CTR Preload}, elle correspond à la concaténation \((Nonce||Counter_i)\) présente sur les descriptions. Cette valeur est composée d'un octet de drapeaux (flags), d'un octet provenant du champs QoS, de l'adresse de l'émetteur sur 6 octets, d'un \(PN\) (6 octets) et d'un compteur \(PL\) sur deux octets initialisé à 1 et qui sera incrémenté pour chaque nouveau chiffrement. Soit un total de 128 bits.
Ce qui donne finalement :
\[M_i = C_i \oplus AES_k(CTR\ Preload)\]
\[C_i = AES_k(CTR\ Preload) \oplus M_i\]
\textit{l'indice \(i\) n'apparaît pas ici mais le compteur \(PL\) est bien présent dans le \(CTR\ Preload\). Ce compteur est incrémenté à chaque nouveau chiffrement.}
\textit{Notons également que lors du chiffrement du MAC (défini dans la partie suivante), le compteur \(PL\) du \(CTR\ Preload\) est égal à \(0\).}
\newpage

\subsubsection{CBC - Cipher Block Chaining}
Chaque bloc à chiffrer subit un XOR avec le bloc chiffré précédent. Pour le premier bloc, un vecteur d'initialisation \(IV\) est utilisé.

\begin{figure}[htp]
    \centering
   \includegraphics[scale=2.3]{ecb_cbc.png}
\end{figure}

\textit{Illustration : à gauche, les données non chiffrées, au centre, le chiffrement utilise le mode ECB et à droite, un autre mode tel que CBC (source : Wikipédia)}

Avec le mode ECB (Electronic codebook), deux blocs identiques donnent le même bloc chiffré (ce qui permet de trouver les blocs clairs réutilisés), ce qui n'est pas le cas avec le mode CBC puisque le bloc chiffré dépend des blocs précédents.
De plus, l'interdépendance des blocs permet d'assurer qu'un bloc de la chaîne ne peut pas être modifié sans conséquences sur les suivants.

\begin{figure}[htp]
    \centering
    \includegraphics[scale=0.59]{cbc.svg.png}
\end{figure}
\textit{(source : Wikipédia)} \\

Notons \(M_i\) le \(i^{eme}\) bloc non chiffré, \(C_i\) le \(i^{eme}\) bloc chiffré avec \(i \geq 1\) et \(C_0 = IV\). La fonction de chiffrement est notée \(AES_k\) et le déchiffrement \(AES^{-1}_k\) avec \(k\) la clé de chiffrement.

\[C_i = AES_k(M_i \oplus C_{i - 1})\]
\begin{figure}[htp]
    \centering
    \includegraphics[scale=0.73]{CBC_decryption.png}
\end{figure}
\textit{(source : Wikipédia)} \\

\[C_i = AES_k(M_i \oplus C_{i - 1})\]
\[AES^{-1}_k(C_i) = M_i \oplus C_{i - 1}\]
\[AES^{-1}_k(C_i) \oplus C_{i - 1} = M_i \oplus C_{i - 1} \oplus C_{i - 1}\]
\[AES^{-1}_k(C_i) \oplus C_{i - 1} = M_i\]

\subsubsection{CBC-MAC - Message Authentication Code}

Ce calcul a pour but de construire un code d'authentification (MAC) en se basant sur le mode CBC afin de contrôler l'intégrité du message ainsi que l'authentification car le processus nécessite de connaitre la clé de chiffrement.

Dans ce cas le vecteur d'initialisation est le résultat du chiffrement par AES d'un \(Nonce\) très similaire au \(CTR\ Preload\). Ce \(Nonce\) est construit par concaténation avec un octet de drapeaux, un octet provenant du champ QoS, l'adresse de l'émetteur sur 6 octets, un \(PN\) sur 6 octets et deux octets contenant la longueur totale du message à chiffrer.

Les 64 premiers bits (bits de poids fort) du dernier bloc chiffré de 128 bits sont conservés pour former le MAC.

Ce MAC permet de vérifier l'intégrité de l'intégralité du message (en-tête comprise) mais dans la trame finale, l'en-tête n'est pas chiffrée.

Lors de la réception, le destinataire déchiffre le message (mode CTR) puis calcule le MAC (mode CBC) qu'il compare au MAC reçu, si les deux résultats correspondent, les données n'ont pas été modifiées et peuvent donc être conservées.

Voici un schéma résumant le fonctionnement de CCMP\footnote{\url{www.researchgate.net/figure/Diagram-of-the-CCMP-encapsulation-process\_fig15\_321759837}}.

\begin{figure}[htp]
    \centering
    \includegraphics[scale=0.35]{CCMP.png}
\end{figure}

\newpage

\subsection{AES}
Advanced Encryption Standard (AES) est une spécification de chiffrement symétrique par blocs définie par la National Institute of Standards and Technology.
La taille des blocs est de 128 bit et la taille de clé peut être de 128,192 ou 256 bits.

\begin{figure}[htp]
    \centering
    \includegraphics[scale=0.47]{tableauAES_2.png}
\end{figure}
\url{https://www.researchgate.net/figure/AES-key-lengths-and-number-of-rounds_tbl2_326557872}
\\
\\

\textit{Ce document traite de la version AES-128} \\

AES utilise une matrice carrée d'ordre 4 pour représenter les données à chiffrer (128 bits soit 16 octets, ce qui correspond au nombre d'éléments de la matrice).
Notons que dans une implémentation, il est également possible de représenter les données comme un tableau unidimensionnel à 16 éléments.

\begin{itemize}
    \item indices 0 à 3 : première colonne
    \item indices 4 à 7 : deuxième colonne
    \item indices 8 à 11 : troisième colonne
    \item indices 12 à 15 : quatrième colonne
\end{itemize}

\begin{figure}[htp]
    \centering
    \includegraphics[scale=0.61]{tab.png}
\end{figure}

Quelle que soit la représentation utilisée, les octets sont considérés comme des éléments de \(\mathbb{F}_{256}\).

Dans la suite de ce document nous utiliserons la représentation matricielle et la matrice sera notée \(S\).
\newpage

\subsubsection{Opérations}

Le corps fini \(\mathbb{F}_{256}\) est muni de deux opérations : l'addition et la multiplication.

\(0\) est l'élément neutre pour l'addition et est l'élément absorbant pour la multiplication. \(1\) est l'élément neutre pour la multiplication.

La multiplication est distributive par rapport à l'addition.

L'addition dans \(\mathbb{F}_{256}\) est simple à comprendre et à implémenter car elle est équivalente à un XOR (OU exclusif).
Ainsi \(0x19 + 0x0d\) est équivalent à \(00011001_2 \oplus 00001101_2 = 00010100_2\) donc \(0x19 + 0x0d = 0x14\).
\newline
La multiplication, quant à elle, est moins directe.
Ici, il est nécessaire de représenter les éléments de \(\mathbb{F}_{256}\) comme étant des polynômes dans \(\mathbb{F}_2[x]/x^8 + x^4 + x^3 + x + 1\) (le polynôme \(x^8 + x^4 + x^3 + x + 1\) est irréductible).
La multiplication se fait en deux étapes :
\begin{itemize}
    \item multiplication des polynômes
    \item recherche du reste de la division par \(x^8 + x^4 + x^3 + x + 1\)
\end{itemize}
Le résultat est le reste de la seconde étape. La recherche du reste étant similaire à celle utilisée dans le calcul de CRC (Contrôle de redondance cyclique), voici un cours expliquant son fonctionnement : \\ \url{https://ensiwiki.ensimag.fr/images/f/f3/Tp\_crc16.pdf}
Un programme en C++ permettant de réaliser ce calcul de reste est disponible dans le répertoire CPP du dépôt GitLab (lien au début de ce dossier).

Calculons \(0x23 * 0x1d\).
dans un premier temps voici les polynômes correspondants :

\(0x19 = 00100011_2\) donc \(x^5 + x + 1\)

\(0x0d = 00011101_2\) donc \(x^4 + x^3 + x^2 + 1\)

\[(x^5 + x + 1)*(x^4 + x^3 + x^2 + 1)\]
\[= x^9 + x^8 + x^7 + x^5 + x^5 + x^4 + x^3 + x + x^4 + x^3 + x^2 + 1\]
dans \(\mathbb{F}_2\), 1 + 1 = 0 ce qui nous donne :
\[x^9 + x^8 + x^7 + x^2 + x + 1\]

cherchons le reste de la division par \(x^8 + x^4 + x^3 + x + 1\)

\[
\begin{matrix}
    1 & 1 & 1 & 0 & 0 & 0 & 0 & 1 & 1 & 1 \\
    1 & 0 & 0 & 0 & 1 & 1 & 0 & 1 & 1 \\
    - & - & - & - & - & - & - & - & - \\
    0 & 1 & 1 & 0 & 1 & 1 & 0 & 0 & 0 & 1 \\
      & 1 & 0 & 0 & 0 & 1 & 1 & 0 & 1 & 1 \\
      & - & - & - & - & - & - & - & - & - \\
      & 0 & 1 & 0 & 1 & 0 & 1 & 0 & 1 & 0 \\
\end{matrix}
\]
le polynôme trouvé est \(x^7 + x^5 + x^3 + x\) soit \(10101010_2 = 0xaa\)
\newline
\textit{Notons que la représentation utilisant les polynômes peut être utilisée pour l'addition également, c'est ce qui a été fait pour calculer ceci :}
\[x^9 + x^8 + x^7 + x^5 + x^5 + x^4 + x^3 + x + x^4 + x^3 + x^2 + 1\]
\[= x^9 + x^8 + x^7 + x^2 + x + 1\]

Les opérations décrites dans cette partie sont utilisées tout au long de ce document.

\subsubsection{SubBytes}
La première étape de l'algorithme consiste à calculer l'inverse multiplicatif de chaque élément de la matrice de départ dans \(\mathbb{F}_{256}\) et à appliquer la transformation affine suivante dans \((\mathbb{F}_{2})^8\) (chaque élément de cet ensemble représente un octet dont les bits sont : \(b_7, b_6, b_5, b_4, b_3, b_2, b_1, b_0\) avec \(b_0\) le bit de poids faible et \(b_7\) le bit de poids fort) :

\[
\begin{bmatrix}
    s_0 \\
    s_1 \\
    s_2 \\
    s_3 \\
    s_4 \\
    s_5 \\
    s_6 \\
    s_7
\end{bmatrix}
=
\begin{bmatrix}
    1 & 0 & 0 & 0 & 1 & 1 & 1 & 1 \\
    1 & 1 & 0 & 0 & 0 & 1 & 1 & 1 \\
    1 & 1 & 1 & 0 & 0 & 0 & 1 & 1 \\
    1 & 1 & 1 & 1 & 0 & 0 & 0 & 1 \\
    1 & 1 & 1 & 1 & 1 & 0 & 0 & 0 \\
    0 & 1 & 1 & 1 & 1 & 1 & 0 & 0 \\
    0 & 0 & 1 & 1 & 1 & 1 & 1 & 0 \\
    0 & 0 & 0 & 1 & 1 & 1 & 1 & 1
\end{bmatrix}
*
\begin{bmatrix}
    b_0 \\
    b_1 \\
    b_2 \\
    b_3 \\
    b_4 \\
    b_5 \\
    b_6 \\
    b_7
\end{bmatrix}
+
\begin{bmatrix}
    1 \\
    1 \\
    0 \\
    0 \\
    0 \\
    1 \\
    1 \\
    0
\end{bmatrix}
\]
\textit{Notons que la matrice utilisée est circulante.}

\[s_0 = b_0 + b_4 + b_5 + b_6 + b_7 + 1\]
\[s_1 = b_0 + b_1 + b_5 + b_6 + b_7 + 1\]
\[s_2 = b_0 + b_1 + b_2 + b_6 + b_7 + 0\]
\[s_3 = b_0 + b_1 + b_2 + b_3 + b_7 + 0\]
\[s_4 = b_0 + b_1 + b_2 + b_3 + b_4 + 0\]
\[s_5 = b_1 + b_2 + b_3 + b_4 + b_5 + 1\]
\[s_6 = b_2 + b_3 + b_4 + b_5 + b_6 + 1\]
\[s_7 = b_3 + b_4 + b_5 + b_6 + b_7 + 0\]

\newpage
ce qui correspond à :

\[s_i = b_i \oplus b_{(i + 4) mod 8} \oplus b_{(i + 5) mod 8} \oplus b_{(i + 6) mod 8} \oplus b_{(i + 7) mod 8} \oplus c_i\]


avec \(c = 0x63 = 01100011_2\).
Il est ici question d'associer à la valeur \(b\) de chaque octet une nouvelle valeur \(s = S(b)\).

D'après le théorème de Lagrange sur les groupes, l'ordre d'un élément d'un groupe est un diviseur du cardinal de ce groupe.
donc pour un élément \(a\) d'ordre \(t\), dans un groupe de cardinal \(n\), on a \(n = k * t, k\in \mathbb{N}\).
donc \(a^n = (a^t)^k = 1^k = 1\). Alors \(a^{n-1}\) est l'inverse de \(a\) car \(a^{n-1} * a = a^n = 1\).
Dans le cas d'AES nous cherchons des inverses dans \(\mathbb{F}_{256} - \{0\} = (\mathbb{F}_{256})^*\) (groupe par rapport à la multiplication car tous les éléments ont un inverse).
\((\mathbb{F}_{256})^*\) est de cardinal 255.
Donc pour tout \(A \in (\mathbb{F}_{256})^*\), nous avons \(A^{254} = A^{-1}\)

Puisque 0 n'a pas d'inverse, nous considérons que sa valeur reste inchangée lors de cette étape et nous avons donc \(S(0) = 0x63\).

\paragraph{Exemple : }
dans cet exemple, un élément de la matrice S est \(b = 90 = 0x5a = 01011010_2\).
\(b \in \mathbb{F}_{256}\)
Nous cherchons l'inverse multiplicatif de \(b\) noté \(b^{-1}\).
Comme nous l'avons vu, \(b^{255} = 1\) et \(b * b^{-1} = 1\) donc \(b^{-1} = b^{254}\)

Calculer des produits d'éléments de \(\mathbb{F}_{256}\) peut être fait avec une fonction de ce type\footnote{\url{https://en.wikipedia.org/wiki/Finite\_field\_arithmetic}} qui utilise la technique de multiplication dite Russe : 
\begin{minted}{c}
uint8_t gmul(uint8_t a, uint8_t b)
{
    uint8_t p = 0;
    while (a && b)
    {
        if (b & 1) 
            p ^= a;

        if (a & 0x80)
            a = (a << 1) ^ 0x11b; // x^8 + x^4 + x^3 + x + 1
        else
            a <<= 1;
        b >>= 1;
    }
	return p;
}
\end{minted}
\newpage
Élevons \(b\) à la puissance 254 :
\begin{minted}{c++}
    uint8_t resultat = gmul(90, 90); // 2
    resultat = gmul(resultat, 90); // 3
    uint8_t p3 = resultat;
    resultat = gmul(resultat,resultat); // 6
    resultat = gmul(resultat, resultat); // 12
    uint8_t p15 = gmul(resultat, p3); // 15
    resultat = gmul(resultat, resultat); // 24
    resultat = gmul(resultat, resultat); // 48
    resultat = gmul(resultat, p15); // 63
    resultat = gmul(resultat, resultat); // 126
    resultat = gmul(resultat, 90); // 127
    resultat = gmul(resultat, resultat); // 254
\end{minted}

Après exécution, nous trouvons \(b^{254} = b^{-1} = 34 = 00100010_2\).

ce qui nous donne :

\[s_0 = 0 + 0 + 1 + 0 + 0 + 1 = 0\]
\[s_1 = 0 + 1 + 1 + 0 + 0 + 1 = 1\]
\[s_2 = 0 + 1 + 0 + 0 + 0 + 0 = 1\]
\[s_3 = 0 + 1 + 0 + 0 + 0 + 0 = 1\]
\[s_4 = 0 + 1 + 0 + 0 + 0 + 0 = 1\]
\[s_5 = 1 + 0 + 0 + 0 + 1 + 1 = 1\]
\[s_6 = 0 + 0 + 0 + 1 + 0 + 1 = 0\]
\[s_7 = 0 + 0 + 1 + 0 + 0 + 0 = 1\]
\newpage
soit \(S(b) = 10111110_2 = 0xbe\).
ce qui correspond bien à la valeur trouvée dans la S-box :

\begin{figure}[htp]
    \centering
   \includegraphics[scale=0.7]{sbox.png}
\end{figure}
\textit{(source : Wikipédia)} \\

En pratique, il est possible de conserver la S-box dans le code source d'un programme pour éviter les calculs, c'est ce qui a été fait dans le noyau Linux\footnote{\url{https://github.com/torvalds/linux/blob/master/lib/crypto/aes.c}} par exemple :
\begin{minted}{c}
static volatile const u8 __cacheline_aligned aes_sbox[] = {
	0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5,
	0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
	0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0,
	0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
	0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc
	...
};
\end{minted}

Toutes les valeurs de la S-box sont différentes et il n'existe aucun \(b \in \mathbb{F}_{256}\) telle que \(S(b) = b\). L'application décrite ici est donc une bijection sans point fixe.
Notons également qu'il n'existe pas non plus d'élément \(b \in \mathbb{F}_{256}\) tel que \(S(b) \oplus b = 0xff\) (soit \(S(b) + b = 0xff)\).

\subsubsection{ShiftRows}

Cette étape modifie les lignes de la matrice obtenue après l'étape SubBytes. Pour cela, des décalages circulaires vers la gauche sont appliqués sur les lignes selon les données suivantes :
\begin{itemize}
    \item ligne 0 : reste inchangée
    \item ligne 1 : 1 rang
    \item ligne 2 : 2 rangs
    \item ligne 3 : 3 rangs (équivalent à un décalage d'un rang vers la droite)
\end{itemize}

\begin{figure}[htp]
    \centering
    \includegraphics[scale=0.525]{shiftrows.pdf}
\end{figure}
\textit{À gauche, la matrice après l'étape SubBytes et à droite le résultat de l'étape ShiftRows.}


\subsubsection{MixColumns}

Une fois les rotations appliquées sur les lignes, nous représentons une colonne de la matrice avec un élément de \((\mathbb{F}_{256})^4\).
\[
\begin{bmatrix}
    a_0 & a_4 & a_8 & a_{12} \\
    a_1 & a_5 & a_9 & a_{13} \\
    a_2 & a_6 & a_{10} & a_{14} \\
    a_3 & a_7 & a_{11} & a_{15}
\end{bmatrix}
\]
donne \((a_0, a_1, a_2, a_3)\), \((a_4, a_5, a_6, a_7)\), \((a_8, a_9, a_{10}, a_{11})\) et \((a_{12}, a_{13}, a_{14}, a_{15})\). \\
La transformation suivante est appliquée pour toutes les colonnes :
\[
\begin{bmatrix}
    b_0 \\
    b_1 \\
    b_2 \\
    b_3
\end{bmatrix}
=
\begin{bmatrix}
    2 & 3 & 1 & 1 \\
    1 & 2 & 3 & 1 \\
    1 & 1 & 2 & 3 \\
    3 & 1 & 1 & 2
\end{bmatrix}
*
\begin{bmatrix}
    a_0 \\
    a_1 \\
    a_2 \\
    a_3
\end{bmatrix}
\textit{La matrice utilisée est circulante.}
\]


\paragraph{Exemple : }

Considérons une matrice dont une des colonnes serait représentée par \(C \in (\mathbb{F}_{256})^4\)
\[C = 
\begin{bmatrix}
    01 \\
    20 \\
    6c \\
    2f
\end{bmatrix}\]
\textit{Les valeurs notées ici sont en hexadécimal.}
\newline
Ceci nous donne donc :
\[
\begin{bmatrix}
    b_0 \\
    b_1 \\
    b_2 \\
    b_3
\end{bmatrix}
=
\begin{bmatrix}
    2 & 3 & 1 & 1 \\
    1 & 2 & 3 & 1 \\
    1 & 1 & 2 & 3 \\
    3 & 1 & 1 & 2
\end{bmatrix}
*
\begin{bmatrix}
    01 \\
    20 \\
    6c \\
    2f
\end{bmatrix}
=
\begin{bmatrix}
    2*01 + 3*20 + 1*6c + 1*2f \\
    1*01 + 2*20 + 3*6c + 1*2f \\
    1*01 + 1*20 + 2*6c + 3*2f \\
    3*01 + 1*20 + 1*6c + 2*2f
\end{bmatrix}
\]
\[
=
\begin{bmatrix}
    2 + 3*20 + 6c + 2f \\
    1 + 2*20 + 3*6c + 2f \\
    1 + 20 + 2*6c + 3*2f \\
    3 + 20 + 6c + 2*2f
\end{bmatrix}
\]

calculons \(3 * 20\), \(2 * 20\), \(3 * 6c\), \(2 * 6c\), \(3 * 2f\) et \(2 * 2f\).

\(0x20 = 00100000_2\) = \(x^5\).

\(0x6c = 01101100_2\) = \(x^6 + x^5 + x^3 + x^2\).

\(0x2f = 00101111_2\) = \(x^5 + x^3 + x^2 + x + 1\).

\(0x03 = 00000011_2\) = \(x + 1\).

\(0x02 = 00000010_2\) = \(x\).

ce qui nous donne :

\[3 * 20 = (x + 1) * x^5 = x^6 + x^5 = 01100000_2 = 0x60\]

\[2 * 20 = x * x^5 = x^6 = 01000000_2 = 0x40\]

\[3 * 6c = (x + 1) * (x^6 + x^5 + x^3 + x^2) = x^7 + x^6 + x^4 + x^3 + x^6 + x^5 + x^3 + x^2\]
\[= x^7 + x^5 + x^4 + x^2 \]
\[= 10110100_2 = 0xb4\]

\[2 * 6c = x * (x^6 + x^5 + x^3 + x^2) = x^7 + x^6 + x^4 + x^3 = 11011000_2 = 0xd8\]

\[3 * 2f = (x + 1) * (x^5 + x^3 + x^2 + x + 1) = x^6 + x^4 + x^3 + x^2 + x + x^5 + x^3 + x^2 + x + 1\]
\[= x^6 + x^5 + x^4 + 1\]
\[= 01110001_2 = 0x71\]

\[2 * 2f = x * (x^5 + x^3 + x^2 + x + 1) = x^6 + x^4 + x^3 + x^2 + x = 01011110_2 = 0x5e\]

\[
\begin{bmatrix}
    2 + 3*20 + 6c + 2f \\
    1 + 2*20 + 3*6c + 2f \\
    1 + 20 + 2*6c + 3*2f \\
    3 + 20 + 6c + 2*2f
\end{bmatrix}
\]
\[
=
\begin{bmatrix}
    2 + 60 + 6c + 2f \\
    1 + 40 + b4 + 2f \\
    1 + 20 + d8 + 71 \\
    3 + 20 + 6c + 5e
\end{bmatrix}
\]
\[
=
\begin{bmatrix}
    21 \\
    da \\
    88 \\
    11
\end{bmatrix}
\]

\subsubsection{Préparation de la clé - Key Schedule}

L'objectif de cette étape est d'obtenir 11 clés (la clé initiale plus une clé par tour, soit un total de 176 octets) à partir de la clé initiale (16 octets).
Comme pour les données à chiffrer, la clé peut être représentée comme une matrice carrée d'ordre 4 et le résultat par une matrice à 4 lignes et 44 colonnes dont les 4 premières colonnes sont celles de la clé initiale. \\
Nous allons voir comment calculer les colonnes suivantes. \\

Il est nécessaire de définir les \(RCon\) (Round constants) qui sont des éléments de \((\mathbb{F}_{256})^4\).
Chaque \(RCon\) correspond à un tour, il est donc nécessaire d'en calculer 10.
\newline
\(RC(i) = x^{i - 1}\) pour \(1 \leq i \leq 10\). \\
Représentons ceci sous la forme d'une matrice à 4 lignes et 10 colonnes.
\[
\begin{bmatrix}
    RC(1) & RC(2) & ... & RC(10) \\
    0 & 0 & ... & 0 \\
    0 & 0 & ... & 0 \\
    0 & 0 & ... & 0 \\
\end{bmatrix}
=
\begin{bmatrix}
    x^0 & x^1 & ... & x^9 \\
    0 & 0 & ... & 0 \\
    0 & 0 & ... & 0 \\
    0 & 0 & ... & 0 \\
\end{bmatrix}
\]
Il est nécessaire de trouver le reste de la division de \(x^9\) par \(x^8 + x^4 + x^3 + x + 1\).
Il en est de même pour \(x^8\) (les autres polynômes ont déjà un degré inférieur à 8).
\newline
Pour \(x^8\) on a \(100000000_2\)
\[
\begin{matrix}
    1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
    1 & 0 & 0 & 0 & 1 & 1 & 0 & 1 & 1 \\
    - & - & - & - & - & - & - & - & - \\
    0 & 0 & 0 & 0 & 1 & 1 & 0 & 1 & 1 \\
\end{matrix}
\]
le reste est \(x^4 + x^3 + x + 1 = 00011011_2 = 0x1b\)
\newline
Pour \(x^9\) on a \(1000000000_2\)
\[
\begin{matrix}
    1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
    1 & 0 & 0 & 0 & 1 & 1 & 0 & 1 & 1 \\
    - & - & - & - & - & - & - & - & - \\
    0 & 0 & 0 & 0 & 1 & 1 & 0 & 1 & 1 & 0 \\
\end{matrix}
\]
le reste est \(x^5 + x^4 + x^2 + x = 00110110_2 = 0x36\)

Voici la matrice finale obtenue :
\[
R =
\begin{bmatrix}
    1 & 2 & 4 & 8 & 10 & 20 & 40 & 80 & 1b & 36 \\
    0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
    0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
    0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
\end{bmatrix}
\]

\textit{La \(i^{eme}\) colonne est utilisée comme RCon pour le \(i^{eme}\) tour.}

\paragraph{Calcul}

pour calculer la \(i^{eme}\) (\(5 \leq i \leq 44\)) colonne de la matrice finale :
\begin{itemize}
    \item si \(i \equiv 1\ \textrm{mod}\ 4\) \\
        pour B, la \((i - 1)^{eme}\) colonne de la matrice représentée ainsi :
        \[
        B = 
        \begin{bmatrix}
            b_0 \\
            b_1 \\
            b_2 \\
            b_3 \\
        \end{bmatrix}
        \]
        A, la \((i - 4)^{eme}\) colonne :
        \[
        A = 
        \begin{bmatrix}
            a_0 \\
            a_1 \\
            a_2 \\
            a_3 \\
        \end{bmatrix}
        \]
        La \(i^{eme}\) colonne C se calcule ainsi :
        \[
        \begin{bmatrix}
            c_0 \\
            c_1 \\
            c_2 \\
            c_3 \\
        \end{bmatrix}
        =
        \begin{bmatrix}
            S(b_1) \\
            S(b_2) \\
            S(b_3) \\
            S(b_0)
        \end{bmatrix}
        +
        \begin{bmatrix}
            a_0 \\
            a_1 \\
            a_2 \\
            a_3 \\
        \end{bmatrix}
        +
        \begin{bmatrix}
            RC(i - 4) \\
            0 \\
            0 \\
            0 \\
        \end{bmatrix}
        \]
        \textit{remarquons l'ordre des éléments dans :}
        \[
        \begin{bmatrix}
            S(b_1) \\
            S(b_2) \\
            S(b_3) \\
            S(b_0)
        \end{bmatrix}
        \]
        \textit{ceci sera expliqué plus précisément dans l'exemple suivant.}


    \item sinon \\
        pour B, la \((i - 1)^{eme}\) colonne et A la \((i - 4)^{eme}\) de la matrice (A et B définis comme précédemment). \\
        La \(i^{eme}\) colonne C se calcule ainsi :
        \[
        \begin{bmatrix}
            c_0 \\
            c_1 \\
            c_2 \\
            c_3 \\
        \end{bmatrix}
        =
        \begin{bmatrix}
            b_0 \\
            b_1 \\
            b_2 \\
            b_3
        \end{bmatrix}
        +
        \begin{bmatrix}
            a_0 \\
            a_1 \\
            a_2 \\
            a_3 \\
        \end{bmatrix}
        \]
\end{itemize}

\paragraph{Exemple}
Choisissons la clé initiale suivante :
\[0 \quad 1 \quad 2 \quad 3 \quad 4 \quad 5 \quad 6 \quad 7 \quad 8 \quad 9 \quad a \quad b \quad c \quad d \quad e \quad f\]
représentée ainsi :
\[
\begin{bmatrix}
    0 & 4 & 8 & c \\
    1 & 5 & 9 & d \\
    2 & 6 & a & e \\
    3 & 7 & b & f \\
\end{bmatrix}
\]
\newline
colonne 5 :
\newline
Nous considérons ici la \(4^{eme}\) colonne.
\[
\begin{bmatrix}
    c \\
    d \\
    e \\
    f
\end{bmatrix}
\]
Nous appliquons une rotation de 1 octet vers la gauche (la forme matricielle n'est qu'une représentation), ce qui nous donne :
\[
\begin{bmatrix}
    d \\
    e \\
    f \\
    c
\end{bmatrix}
\]
Effectuons maintenant la même substitution que celle opérée lors de l'étape SubBytes. Pour cela, cherchons les éléments correspondants dans la S-box.
\[
\begin{bmatrix}
    S(d) \\
    S(e) \\
    S(f) \\
    S(c)
\end{bmatrix}
=
\begin{bmatrix}
    d7 \\
    ab \\
    76 \\
    fe
\end{bmatrix}
\]
Ici, nous sommes au premier tour donc la valeur RCon recherchée correspond à la première colonne de la matrice \(R\).
\[
\begin{bmatrix}
    d7 \\
    ab \\
    76 \\
    fe
\end{bmatrix}
+
\begin{bmatrix}
    0 \\
    1 \\
    2 \\
    3
\end{bmatrix}
+
\begin{bmatrix}
    1 \\
    0 \\
    0 \\
    0
\end{bmatrix}
=
\begin{bmatrix}
    d6 \\
    aa \\
    74 \\
    fd
\end{bmatrix}
\]
colonne 6 :
\[
\begin{bmatrix}
    d6 \\
    aa \\
    74 \\
    fd
\end{bmatrix}
+
\begin{bmatrix}
    4 \\
    5 \\
    6 \\
    7
\end{bmatrix}
=
\begin{bmatrix}
    d2 \\
    af \\
    72 \\
    fa
\end{bmatrix}
\]
colonne 7 :
\[
\begin{bmatrix}
    d2 \\
    af \\
    72 \\
    fa
\end{bmatrix}
+
\begin{bmatrix}
    8 \\
    9 \\
    a \\
    b
\end{bmatrix}
=
\begin{bmatrix}
    da \\
    a6 \\
    78 \\
    f1
\end{bmatrix}
\]
colonne 8 :
\[
\begin{bmatrix}
    da \\
    a6 \\
    78 \\
    f1
\end{bmatrix}
+
\begin{bmatrix}
    c \\
    d \\
    e \\
    f
\end{bmatrix}
=
\begin{bmatrix}
    d6 \\
    ab \\
    76 \\
    fe
\end{bmatrix}
\]
colonne 9 :
\[
\begin{bmatrix}
    d6 \\
    ab \\
    76 \\
    fe
\end{bmatrix}
\textrm{rotation :}
\begin{bmatrix}
    ab \\
    76 \\
    fe \\
    d6 \\
\end{bmatrix}
\]
\[
\begin{bmatrix}
    S(ab) \\
    S(76) \\
    S(fe) \\
    S(d6)
\end{bmatrix}
=
\begin{bmatrix}
    62 \\
    38 \\
    bb \\
    f6
\end{bmatrix}
\]
\[
\begin{bmatrix}
    62 \\
    38 \\
    bb \\
    f6
\end{bmatrix}
+
\begin{bmatrix}
    d6 \\
    aa \\
    74 \\
    fd
\end{bmatrix}
+
\begin{bmatrix}
    2 \\
    0 \\
    0 \\
    0
\end{bmatrix}
=
\begin{bmatrix}
    b6 \\
    92 \\
    cf \\
    b
\end{bmatrix}
\]

colonne 10 :

\[
\begin{bmatrix}
    b6 \\
    92 \\
    cf \\
    b
\end{bmatrix}
+
\begin{bmatrix}
    d2 \\
    af \\
    72 \\
    fa
\end{bmatrix}
=
\begin{bmatrix}
    64 \\
    3d \\
    bd \\
    f1
\end{bmatrix}
\]

colonne 11 :

\[
\begin{bmatrix}
    64 \\
    3d \\
    bd \\
    f1
\end{bmatrix}
+
\begin{bmatrix}
    da \\
    a6 \\
    78 \\
    f1
\end{bmatrix}
=
\begin{bmatrix}
    be \\
    9b \\
    c5 \\
    0
\end{bmatrix}
\]

colonne 12 :

\[
\begin{bmatrix}
    be \\
    9b \\
    c5 \\
    0
\end{bmatrix}
+
\begin{bmatrix}
    d6 \\
    ab \\
    76 \\
    fe
\end{bmatrix}
=
\begin{bmatrix}
    68 \\
    30 \\
    b3 \\
    fe
\end{bmatrix}
\]
\[...\]
\[...\]
\[...\]

Le résultat final est :
\[
\begin{bmatrix}
    0 & 4 & 8 & c & d6 & d2 & da & d6 & b6 & 64 & be & 68 & ... \\
    1 & 5 & 9 & d & aa & af & a6 & ab & 92 & 3d & 9b & 30 & ... \\
    2 & 6 & a & e & 74 & 72 & 78 & 76 & cf & bd & c5 & b3 & ... \\
    3 & 7 & b & f & fd & fa & f1 & fe & b  & f1 & 0  & fe & ...
\end{bmatrix}
\]

\begin{figure}[htp]
    \centering
    \includegraphics[scale=0.385]{key_schedule.pdf}
\end{figure}

\subsubsection{Add RoundKey}
Cette étape consiste simplement à additionner La matrice \(S\) et une matrice représentant une clé (clé initiale ou clé de tour).

\[
\begin{bmatrix}
    a_0 & a_4 & a_8 & a_{12} \\
    a_1 & a_5 & a_9 & a_{13} \\
    a_2 & a_6 & a_{10} & a_{14} \\
    a_3 & a_7 & a_{11} & a_{15} \\
\end{bmatrix}
+
\begin{bmatrix}
    k_0 & k_4 & k_8 & k_{12} \\
    k_1 & k_5 & k_9 & k_{13} \\
    k_2 & k_6 & k_{10} & k_{14} \\
    k_3 & k_7 & k_{11} & k_{15} \\
\end{bmatrix}
\]

Le résultat est ensuite affecté à la matrice \(S\).
\newpage
\subsubsection{Algorithme de haut niveau}
Tous les composants d'AES étant maintenant décrits, voici son fonctionnement général :
\begin{minted}{c}
    AddRoundKey; //avec la clé initiale et la matrice S
    pour i variant_de 1 a 9 faire
        SubBytes;
        ShiftRows;
        MixColumns;
        AddRoundKey; //avec la clé du i-ème tour et la matrice S
    ffaire

    //le tour final est différent (10ème tour)
    SubBytes;
    ShiftRows;
    //pas de MixColumns dans le dernier tour
    AddRoundKey;
\end{minted}
Le déchiffrement est effectué en faisant toutes ces étapes dans le sens inverse.

\end{document}
