\documentclass[main.tex]{subfiles}
\begin{document}

\section {WEP}

\subsection{Introduction aux failles du protocole}
Le WEP est malheureusement un protocole très vulnérable en raison de ses caractéristiques cryptographiques de qualité insuffisante. Déjà en 1995, des chercheurs ont pu démontrer des failles dans l'algorithme du RC4 (Wagner, Roos,...) utilisé dans le protocole WEP. Mais ce dernier restait encore un protocole plutôt fiable et c'est à partir du début des années 2000 que de plus en plus de faiblesses sont révélées, avec des articles et des publications sur le sujet. Puis en 2004-2005, des attaques montrent la quasi obsolescence du protocole.\\

Les failles suivantes ont été trouvées dans le protocole WEP :
\begin{itemize}
    \item En terme de confidentialité, la partie secrète de la clé est la même pour tout le monde et la partie envoyée en clair (IV) a 24 bits (la clé est réutilisée après 224 trames). Les STA (Spanning Tree Algorithm) utilisent le même IV quand elles commencent à transmettre. Il existe des méthodes qui permettent de découvrir la clé WEP à partir d’un certain nombre de trames interceptées. Le paradoxe des anniversaires fait que dès que l'on a capturé environ 5000 paquets, on a plus d'une chance sur deux d'avoir utilisé la même clé.
    \item Concernant les doublons, aucune protection n'est mise en place.
    \item Le contrôle d’intégrité n’est pas cryptographiquement sûr; l’utilisation du CRC est satisfaisante pour des erreurs aléatoires mais pas pour les erreurs délibérées et il est possible de modifier des messages sans être détecté (changer des données et ICV).
    \item Enfin, aucune authentification bidirectionnelle n'est prévue et seule la station (STA) est authentifiée. Le challenge d'authentification à clé partagée est envoyé en clair ce qui provoque une exposition dangereuse à des attaques connues.
\end{itemize}

\subsection{Différentes attaques sur le protocole}
Les attaques ont donc commencé dès 2001 (ces attaques avaient pour but d'exploiter les failles laissées par le protocole WEP). Une multitude d'attaque et de méthode existent aujourd'hui, par exemple:
\begin{itemize}
    \item L'attaque des "mots de passe" par dictionnaire 
    \item L'attaque "man in the middle" 
    \item Packet injection
    \item Attaque FMS
    \item Fake authentication
    \item L'attaque chopchop de KoreK
    \item Bit flipping sur la somme de contrôle CRC-32
    \item Plaintext attack
    \item Attaque par fragmentation
    \item Attaque par IV connu
    \item Utilisation de vecteurs d’initialisation IV « faibles »
    \item Collision des vecteurs d’initialisation IV
    \item Réutilisation du vecteur d’initialisation IV\\
\end{itemize}
Dans ce document, nous allons nous intéresser plus particulièrement à 2 de ces attaques : Packet injection et FMS.\\ \\
L'attaque par injection de paquets a pour but d'accélérer la production d'IV nécessaire à l'obtention de la clé. Cette attaque est très efficace lorsqu'une station connectée génère des requêtes ARP. L'attaquant essaie de trouver un paquet ARP chiffré afin de l'utiliser pour l'injection. 
L'injection de paquets peut être considérée comme une véritable attaque contre le WEP, car ce protocole n'a pas été conçu pour résister à ce type de menace. 
Un paquet envoyé dans un réseau protégé par le WEP et qui a été intercepté par un attaquant peut ensuite être injecté à nouveau dans le réseau,
à condition que la clé n'ait pas été modifiée et que la station d'envoi d'origine soit toujours dans le réseau. \\

Autre type d'attaque, l'attaque FMS (pour les initiales des noms des chercheurs FLURHER, MANTIN et SHAMIR). En 2001, les chercheurs Scott FLURHER, Itsik MANTIN et Adi SHAMIR ont publié une attaque de récupération de clé qui consiste à exploiter le fait que le protocole WEP possède des IVs faibles. C'est à dire que l'attaquant va récupérer des paquets chiffrés en écoutant passivement le trafic et mémoriser les 3 premiers octets publics. Il connaît alors les $l$ premiers octets d'une clé RC4 utilisée pour générer un keystream $X$. Il peut ainsi commencer à effectuer les premières étapes du RC4-KSA (Key scheduling algorithm) pour connaître $S_{l}$ et $j_l$. Ensuite, $j_{l+1} = j_l + K[l] + S_l[l]$ et une permutation est faite entre $S_l[j_{l+1}]$ et $S_l[l]$. Mais si l'attaquant pouvait connaître $S_{l+1}[l]$ il pourrait alors obtenir $K[l]$ en faisant $S_{l}^{-1}[S_{l+1}[l]]-j_l-S_l[l]$.\\
Les 3 chercheurs ont alors une méthode pour l'obtenir : supposons que les conditions suivantes se maintiennent après les $l$ premières étapes du RC4-KSA (les conditions 3 et 4 ont été introduites plus tard, par Korek pour améliorer l'efficacité de l'attaque):\\
\begin{itemize}
    \item 1. $S_l[1] < l$
    \item 2. $S_l[1] + S_l[S_l[1]] = l$
    \item 3. $S_{l}^{-1}[X[0]]\ne 1$
    \item 4. $S_{l}^{-1}[X[0]] \ne S_l[1]$ \\
\end{itemize}
Ensuite, une valeur $k$ à laquelle on affecte $S_l[j_{l+1}]$ pour après la permuter avec $S_{l+1}[l]$. Si j change aléatoirement pendant le reste de l'algorithme, les valeurs $S[1]$, $S[S[1]]$ et $S[l]$ ne seront pas modifiées avec une probabilité d'environ $(\frac{1}{e})^3 \simeq 5/100$ pour le reste de l'algorithme. Quand le premier octet de sortie est produit par le RC4-PRGA (Pseudo-random generation algorithm), $j$ prendra la valeur $S_n[1]$ puis $S_n[1]$ et $S_n[j]$ seront échangés. Après cela, $S_l[1] + S_l[S_l[1]] = l$ tient toujours et le premier octet de sortie du RC4-PRGA $X[0]$ sera alors $S[l]$. Ensuite, si ces 4 conditions sont toujours vérifiées, la fonction suivante: \\
\begin{itemize}
    \item $F_{fms}(K[0],...,K[l-1],X[0]) = S_{1}^{-1}[X[0]] - j_l - S_l[l]$ \\
\end{itemize}
prendra alors la valeur de $K[l]$ avec une probabilité de  $(\frac{1}{e})^3 \simeq 5/100$. Ainsi, grâce à ces conditions et cette fonction, on peut donc lancer une attaque de récupération complète de la clé sur le WEP. L'attaquant, lorsqu'il a capturé suffisamment de paquets, sélectionne où se trouve les conditions résolues et calcule $F_{fms}$ pour ces paquets.
Chacun des résultats pourra être considéré comme un vote pour la valeur de $Rk[0]$ et l'attaquant devra choisir laquelle correspond bien à la vraie valeur de $Rk[0]$. Si la décision est bonne, alors il connaît les premiers $l$ octets de chaque paquet et il fait de même pour $Rk[1]$ et ainsi de suite. Une fois tous les octets de $Rk$ obtenus, l'attaquant vérifie alors la clé qu'il a pu former afin de savoir si elle est correcte; dans l'affirmative l'attaque a réussi.\\
 
 En moyenne il fallait au moins 4 000 000 de paquets chiffrés pour retrouver la clé avec une probabilité supérieure à 1/2.\\ \\

Autre type d'attaque, Korek en 2004 (qui introduit en même temps l'attaque chopchop). Korek est un hacker qui a publié une série de 16 attaques sur le WEP pour retrouver la clé de chiffrement. Pour cela la structure de l'attaque est la même que pour l'attaque FMS mais celle de Korek permet de réduire le nombre moyen de paquets nécessaire à 700 000 pour une probabilité qui est autour de 1/2. \\
Toutefois c'est en 2005 qu'une équipe du FBI démontre la possibilité de rentrer dans un réseau protégé par le protocole WEP en moins de 5 minutes avec des outils pratiquement à la portée de tous. En effet, ces agents du FBI auraient utilisé, entre autre, Kismet, un logiciel de détection de réseaux et Airodump, qui fonctionne avec Aircrack, pour capturer les paquets. Durant cette démonstration, ils ont notamment utilisé une attaque par rejeu.\\

Cette démonstration prouvait alors définitivement l'insuffisance de ce protocole pour assurer la sécurité d'un réseau (le WPA existait déjà à ce moment-là).\\
Aujourd'hui il est même possible d'entrer dans un réseau protégé par le protocole WEP en moins d'une minute grâce à de bons outils et c'est la raison pour laquelle il n'est presque plus utilisé sur les réseaux WIFI.\\
\end{document}
