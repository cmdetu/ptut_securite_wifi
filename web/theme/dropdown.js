var dropdown = document.getElementsByClassName("dropdown-btn");
var i;


for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function () {
    this.classList.toggle("active");
    var parentActive = this.classList.contains("active")
    this.childNodes.forEach((currentValue) => {
      try {

        if (currentValue.classList.contains("arrowUpDown")) {

          if (parentActive) {
            currentValue.textContent = '↑'
          } else {
            currentValue.textContent = '↓'

          }
        }
      } catch{

      }
    })

    var dropdownContent = this.nextElementSibling;
    dropdownContent.classList.toggle("cache")
  });
}
