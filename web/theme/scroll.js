/* 
   affiche le H2 en haut à gauche quand le div en dessous est visible mais pas le H2
*/

let titles = [];
let copiedTitles = [];
let contentAfterTitle = [];

function checkVisible(elm) {
    if (elm && elm.getBoundingClientRect) {
        let rect = elm.getBoundingClientRect();
        var viewHeight = Math.max(
            document.documentElement.clientHeight,
            window.innerHeight
        );
        return !(rect.bottom < 0 || rect.top - viewHeight >= 0);
    }
    return false;
}

function init() {
    titles = [];
    copiedTitles = [];
    contentAfterTitle = [];

    let selected = document.querySelectorAll("h3");
    for (let i = 0; i < selected.length; ++i) {
        titles.push(selected[i]);
        let copied = titles[i].cloneNode(true);
        let spanToRemove = undefined;
        copied.childNodes.forEach((currentValue) => {
            try {
                if (currentValue.classList.contains("arrowUpDown")) {
                    spanToRemove = currentValue;
                }
            } catch {}
        });
        try {
            copied.removeChild(spanToRemove);
        } catch (error) {}

        copied.classList.add("top-corner");
        copied.classList.add("hidden");

        copiedTitles.push(document.body.appendChild(copied));
        let next = selected[i].nextSibling;
        let after = [next];
        while (next && next.nodeName != "H3") {
            next = next.nextSibling;
            after.push(next);
        }

        contentAfterTitle.push(after);
    }
}

document.addEventListener("scroll", (ev) => {
    let titlesLength = titles.length;
    for (let i = 0; i < titlesLength; ++i) {
        let title = titles[i];
        let content = contentAfterTitle[i];
        let titleCopied = copiedTitles[i];
        let titleVisible = checkVisible(title);
        if (titleCopied && content) {
            if (content.length && content.length > 0) {
                let acontentIsVisible = false;
                let contentLength = content.length;
                for (let j = 0; j < contentLength; ++j) {
                    if (checkVisible(content[j])) {
                        acontentIsVisible = true;
                        break;
                    }
                }
                if (acontentIsVisible && !titleVisible)
                    titleCopied.classList.remove("hidden");
                else titleCopied.classList.add("hidden");
            }
        }
    }
});

init();
